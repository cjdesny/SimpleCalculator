/*
        A program to demonstrate android development
        using a SimpleCalculator that performs basic calculations
    */

package com.it.simplecalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;


import java.util.ArrayList;


public class SimpleCalculator extends AppCompatActivity {


    public static final int MAX_COMPUTATION = 9;
    public static final String REFRESH = "";


    private ArrayList<String> vars;
    private ArrayList<Character> operators;

    private TextView expressionDisplay, input_output_display, operatorOutput;
    private Button bt_1, bt_2, bt_3, bt_4, bt_5, bt_6, bt_7, bt_8, bt_9, bt_0;
    private Button backspace, bt_period, bt_equal, bt_substitute, bt_add, bt_multiply, bt_divide;

    private boolean isNVar = false;
    private boolean isStackEmpty = true;


    @Override
    protected void onCreate(Bundle lastState) {
        super.onCreate(lastState);
        setContentView(R.layout.activity_simple_calculator);

        vars = new ArrayList<>();
        operators = new ArrayList<>();

        if(lastState != null)
            return;



                    //Find and cast xml resource objects to matching Java class
        expressionDisplay = (TextView) findViewById(R.id.input_display);
        input_output_display = (TextView) findViewById(R.id.input_output_display);
        operatorOutput = (TextView) findViewById(R.id.operatorOutput);


        backspace = (Button) findViewById(R.id.backspace);
        bt_7 = (Button) findViewById(R.id.bt_7);
        bt_8 = (Button) findViewById(R.id.bt_8);
        bt_9 = (Button) findViewById(R.id.bt_9);
        bt_4 = (Button) findViewById(R.id.bt_4);
        bt_5 = (Button) findViewById(R.id.bt_5);
        bt_6 = (Button) findViewById(R.id.bt_6);
        bt_1 = (Button) findViewById(R.id.bt_1);
        bt_2 = (Button) findViewById(R.id.bt_2);
        bt_3 = (Button) findViewById(R.id.bt_3);
        bt_period = (Button) findViewById(R.id.bt_period);
        bt_0 = (Button) findViewById(R.id.bt_0);
        bt_equal = (Button) findViewById(R.id.bt_equal);
        bt_substitute = (Button) findViewById(R.id.bt_substitute);
        bt_add = (Button) findViewById(R.id.bt_add);
        bt_multiply = (Button) findViewById(R.id.bt_multiply);
        bt_divide = (Button) findViewById(R.id.bt_divide);



        Listener btListener = new Listener();

        backspace.setOnClickListener(btListener);
        bt_7.setOnClickListener(btListener);
        bt_8.setOnClickListener(btListener);
        bt_9.setOnClickListener(btListener);
        bt_4.setOnClickListener(btListener);
        bt_5.setOnClickListener(btListener);
        bt_6.setOnClickListener(btListener);
        bt_1.setOnClickListener(btListener);
        bt_2.setOnClickListener(btListener);
        bt_3.setOnClickListener(btListener);
        bt_period.setOnClickListener(btListener);
        bt_0.setOnClickListener(btListener);
        bt_equal.setOnClickListener(btListener);
        bt_substitute.setOnClickListener(btListener);
        bt_add.setOnClickListener(btListener);
        bt_multiply.setOnClickListener(btListener);
        bt_divide.setOnClickListener(btListener);

    }


    public void push(char operator){


        if( vars.size() < MAX_COMPUTATION ){  //Push if computation is still in range



            String tempLastVar = getInput();
            String tempExpression = getExpression();


            if(!isInputEmpty()){    //Collect current input'd var if input isn't empty




                //Dynamic displaying of data
                if(isExpressionEmpty()){


                    if(getInput().startsWith("-"))
                        isNVar = true;


                    if(!isOperatorEmpty() && isNVar){

                        if(!getInput().startsWith("-")) {

                            tempExpression = String.valueOf(getOperator());
                            tempLastVar = tempExpression.concat(tempLastVar);

                            vars.add(tempLastVar);
                            operators.add(operator);

                        }else{
                            vars.add(tempLastVar);
                            operators.add(operator);

                        }

                        setExpression("(" + tempLastVar + ")");
                        setOperator(operator);
                        setOutput(REFRESH);

                        isNVar = false;

                    }else{

                        vars.add(tempLastVar);
                        operators.add(operator);

                        setExpression(tempLastVar);
                        setOperator(operator);
                        setOutput(REFRESH);

                    }
                }
                else if(!isExpressionEmpty()){


                    if(getInput().startsWith("-"))
                        isNVar = true;

                    if(isStackEmpty)
                        clearDisplay();


                    if( !isNVar ){

                        vars.add(tempLastVar);
                        operators.add(operator);


                        String tempExpressionCon;


                        if(operators.size() > 1) {

                            char tempPreviousOp;
                            tempPreviousOp = operators.get(operators.size() - 2);
                            tempExpressionCon = tempExpression.concat(String.valueOf(tempPreviousOp))
                                                .concat(tempLastVar);

                        }
                        else {

                            tempExpressionCon = tempLastVar;
                        }

                        setExpression(tempExpressionCon);
                        setOperator(operator);
                        setOutput(REFRESH);

                    }else{
                        if(!tempLastVar.startsWith("-"))
                            tempLastVar = String.valueOf(getOperator()).concat( getInput() );

                         vars.add(tempLastVar);
                        operators.add(operator);

                        String tempExpressionCon;

                        if(operators.size() > 1) {

                            char tempPreviousOp;

                            tempPreviousOp = operators.get(operators.size() - 2);
                            tempExpressionCon = tempExpression.concat(String.valueOf(tempPreviousOp))
                                    .concat("(" + tempLastVar + ")");

                        }
                        else {

                            tempExpressionCon = "(" + tempLastVar + ")";
                        }

                        setExpression(tempExpressionCon);
                        setOperator(operator);
                        setOutput(REFRESH);

                        isNVar = false;
                    }

                    isStackEmpty = false;
                }
              }
         }
    }


    //Method for c function
    public void deleteLast(){

        String tempLastVar = getInput();
        String tempSub;
        String tempExpression = "";
        char tempLastOperator = ' ';

        //Delete directly from input if it's not empty
        if(!isInputEmpty()){
                tempSub = tempLastVar.substring(0, tempLastVar.length() - 1);

            if(tempLastVar.length() < 1){

                setOperator(REFRESH);
                setOutput(REFRESH);

            }
            else
                setOutput(tempSub);

            }
            else if(!vars.isEmpty() && isInputEmpty()){        //Delete from stack

            setOperator(REFRESH);

            tempLastVar = vars.remove(vars.size() - 1);
            operators.remove(operators.size() - 1);


            if(tempLastVar.startsWith("-"))
                isNVar = true;

            else
                isNVar = false;



            for(int v = 0, o = 0; v < vars.size() && o < operators.size(); v++, o++){

                tempExpression += vars.get(v);

                if( (operators.size() - 1) != o )
                    tempExpression += operators.get(o);

                else
                    tempLastOperator = operators.get(o);

            }

            setExpression(tempExpression);
            setOperator(tempLastOperator);
            setOutput(tempLastVar);

        }else if(isStackEmpty)
            clearDisplay();
        else
            setOperator("");
    }

    //Method to set calculated value to display
    public void setResult(){

        String tempAnsExpression = "";


        if(!isExpressionEmpty() && !isInputEmpty()){


            if( !isOperatorEmpty() && isNVar){

                String tempInput = String.valueOf( getOperator() ).concat( getInput() );
                vars.add(tempInput);


                double answer = compute();


                if(!isStackEmpty){
                    tempAnsExpression = getExpression().concat( String.valueOf( operators.get(operators.size() - 1) ) )
                            .concat( "(" + vars.get( vars.size() - 1 ) + ")" );
                }

                if(isStackEmpty)
                    clearDisplay();


                setExpression(tempAnsExpression);
                setOperator(REFRESH);
                setOutput(String.valueOf(answer));

            }
            else if(!isOperatorEmpty()){

                vars.add(getInput());

                double answer = compute();

                tempAnsExpression = getExpression().concat( String.valueOf( getOperator() ) )
                        .concat( getInput() );


                if(isStackEmpty)
                    clearDisplay();


                setExpression(tempAnsExpression);
                setOperator(REFRESH);
                setOutput(String.valueOf(answer));

            }
        }
        else if(!isExpressionEmpty() && isInputEmpty()){

            if(isStackEmpty)
                clearDisplay();


            operators.remove(operators.size() - 1);


            double answer = compute();

            setOperator(REFRESH);
            setOutput(String.valueOf(answer));
            }
     }


    //Method to calculate values
    public double compute(){

        double tempAnswer;
        double tempVarDouble;
        double [] rawVars = new double[vars.size()];


                for(int v = 0; v < vars.size(); v++){

                    String tempVar = vars.get(v);


                    if(tempVar.startsWith("-")){

                        tempVarDouble = getFusedVar(tempVar);
                        rawVars[v] = -tempVarDouble;

                    }
                     else {

                        rawVars[v] = Double.valueOf(tempVar);
                    }

                }


                tempAnswer = rawVars[0];


            for(int v = 1, o = 0; v < rawVars.length && o < operators.size(); v++, o++){


                switch(operators.get(o)){

                    case '+' :

                        tempAnswer += (rawVars[v]);

                        break;

                    case '-' :

                        tempAnswer -= (rawVars[v]);

                        break;

                    case '*' :

                        if(tempAnswer == 0){

                            tempAnswer = 1;
                            tempAnswer *= (rawVars[v]);

                        }else

                            tempAnswer *= (rawVars[v]);

                        break;

                    case '/' :

                        if( rawVars[v] == 0 )
                            break;

                        else
                            tempAnswer /= (rawVars[v]);

                    default :

                        break;
                 }
           }


        vars.clear();
        operators.clear();
        isStackEmpty = true;
        isNVar = false;

        return tempAnswer;
    }


    //Method to get var if fused
    public double getFusedVar(String fusedVar){

        String subFusedVar = fusedVar.substring(1);

        return Double.valueOf(subFusedVar);
    }



    //Method to set input values to display
    public void setInput(char input){

        String temp = getInput();
        String concatTemp = temp.concat( String.valueOf(input) );

        input_output_display.setText(concatTemp);

    }

    //Method to set current operator for display
    public void setOperator(String operator){

        operatorOutput.setText(operator);

    }

    //Listener to listen to all bt clicks
    public class Listener implements Button.OnClickListener{

        @Override
        public void onClick(View button){

            int id = button.getId();
            char tempInput;

            switch(id){

                case R.id.bt_0 :
                    tempInput = bt_0.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_1 :
                    tempInput = bt_1.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_2 :
                    tempInput = bt_2.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_3 :
                    tempInput = bt_3.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_4 :
                    tempInput = bt_4.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_5 :
                    tempInput = bt_5.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_6 :
                    tempInput = bt_6.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_7 :
                    tempInput = bt_7.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_8 :
                    tempInput = bt_8.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_9 :
                    tempInput = bt_9.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_period :
                    tempInput = bt_period.getText().toString().charAt(0);
                    setInput(tempInput);
                    break;

                case R.id.bt_substitute :

                    if(!isInputEmpty()){

                        tempInput = bt_substitute.getText().toString().charAt(0);
                        push(tempInput);

                    }
                   else if(isInputEmpty() && isExpressionEmpty()){

                        isNVar = true;

                        tempInput = bt_substitute.getText().toString().charAt(0);
                        setOperator(tempInput);

                        break;

                    }
                    else if(isInputEmpty() && !isExpressionEmpty()){

                        tempInput = bt_substitute.getText().toString().charAt(0);

                            if(operators.size() > 1){
                                if(operators.get(operators.size() - 1) != tempInput){

                                    updateOperators(tempInput);
                                    setOperator(tempInput);

                                }
                                else{

                                    isNVar = true;
                                    setOperator(tempInput);
                                    }
                            }
                    }

                     break;

                case R.id.bt_equal :

                    if(!isInputEmpty()){

                        String tempInput_ = getInput();

                        if(!vars.isEmpty())

                             setResult();
                        else

                            setOutput(tempInput_);

                            break;
                    }


                case R.id.bt_add :

                        if(!isInputEmpty()){

                            tempInput = bt_add.getText().toString().charAt(0);
                            push(tempInput);

                            break;

                        }else if(isInputEmpty() && !isExpressionEmpty()){

                            if(isNVar)
                                isNVar = false;

                            tempInput = bt_add.getText().toString().charAt(0);

                            updateOperators(tempInput);
                            setOperator(tempInput);

                            break;
                        }


                case R.id.bt_multiply :

                    if(!isInputEmpty()){

                        tempInput = bt_multiply.getText().toString().charAt(0);
                        push(tempInput);

                        break;

                    }
                    else if(isInputEmpty() && !isExpressionEmpty()){

                        if(isNVar)
                            isNVar = false;

                        tempInput = bt_multiply.getText().toString().charAt(0);

                        updateOperators(tempInput);
                        setOperator(tempInput);

                        break;
                    }


                case R.id.bt_divide :

                    if(!isInputEmpty()){

                        tempInput = bt_divide.getText().toString().charAt(0);
                        push(tempInput);

                        break;

                    }
                    else if(isInputEmpty() && !isExpressionEmpty()){

                        if(isNVar)
                            isNVar = false;

                        tempInput = bt_divide.getText().toString().charAt(0);

                        updateOperators(tempInput);
                        setOperator(tempInput);

                        break;
                    }


                case R.id.backspace :

                    deleteLast();

                    break;

                default:

                    break;
            }
        }
    }


    //Method to check if input is empty
    public boolean isInputEmpty(){

        boolean state;
        String temInputStream = input_output_display.getText().toString();

        state = TextUtils.isEmpty(temInputStream);

        return state;
    }



    //Method to get input'd values from display
    public String getInput(){
        return input_output_display.getText().toString();
    }


    //Method to set result to display
    public void setOutput(String dataResult){
        input_output_display.setText(dataResult);
    }


    //Method to set expression display data
    public void setExpression(String expression){
        expressionDisplay.setText(expression);
    }

    //Method to get expression data from display
    public String getExpression(){
        return expressionDisplay.getText().toString();
    }


    //Method to check if expression display is empty
    public boolean isExpressionEmpty(){
        String state = expressionDisplay.getText().toString();
        return TextUtils.isEmpty(state);
    }


    //Method to set operator on display
    public void setOperator(char operator){
        operatorOutput.setText(String.valueOf(operator));
    }


    //Method to get operator value from display
    public char getOperator(){
        return operatorOutput.getText().charAt(0);
    }


    //Method to check if operator display is empty
    public boolean isOperatorEmpty(){
        String state = operatorOutput.getText().toString();
        return  TextUtils.isEmpty(state);
    }


    //Method to update operators
    public void updateOperators(char operatorUpdate){
        operators.set( (operators.size() - 1), operatorUpdate);
    }


    //Method to refresh display blank
    private void clearDisplay(){
        expressionDisplay.setText("");
        operatorOutput.setText("");
        input_output_display.setText("");
    }
}